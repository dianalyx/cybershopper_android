package com.yuexin.cybershopper.baseCase;

import com.yuexin.cybershopper.R;
import com.yuexin.cybershopper.activity.AboutActivity;
import com.yuexin.cybershopper.activity.GDActivity;
import com.yuexin.cybershopper.activity.MLActivity;
import com.yuexin.cybershopper.activity.SettingActivity;
import com.yuexin.cybershopper.activity.TCOGActivity;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

public abstract class BaseActivity extends Activity implements
		View.OnClickListener {

	public static PopupWindow popupWindow;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		initview();
//		View popView = getLayoutInflater().inflate(R.layout.popw_menu, null,
//				false);
//		popupWindow = new PopupWindow(popView, 230, 1200, true);
	}

	private void initview() {
		loadViewLayout();
		findViewById();
		setListener();
		processLogic();
	}

	protected abstract void onClickEvent(View view);

	protected abstract void findViewById();

	protected abstract void loadViewLayout();

	protected abstract void processLogic();

	protected abstract void setListener();

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		onClickEvent(v);

		
		 
		switch (v.getId()) {
		case R.id.popmenu_tv1:
			System.out.println("xxxxxx");
		
//			disMiss(popupWindow);
//			popupWindow = null;
			openUI(GDActivity.class);
			System.out.println("yyyyyy");
			break;
		case R.id.popmenu_tv2:
			System.out.println("xxxxxx1");
			
//			disMiss(popupWindow);
//			popupWindow = null;
			 openUI(TCOGActivity.class);
			System.out.println("yyyyyy");
			break;
		case R.id.popmenu_tv3:
			System.out.println("xxxxxx2");
			disMiss(popupWindow);
			 openUI(MLActivity.class);
			System.out.println("yyyyyy");
			break;
		case R.id.popmenu_tv4:
			System.out.println("xxxxxx3");
			disMiss(popupWindow);
			openUI(SettingActivity.class);
			System.out.println("yyyyyy");
			break;
		case R.id.popmenu_tv5:
			System.out.println("xxxxxx4");
			disMiss(popupWindow);
			 openUI(AboutActivity.class);
			System.out.println("yyyyyy");
			break;

		default:
			break;
		}
	}

	public void initPoppuWindow(View v, int tag) {
		View popView = getLayoutInflater().inflate(R.layout.popw_menu, null,
				false);
		popupWindow = new PopupWindow(popView, 230, 1200, true);
		popupWindow.setAnimationStyle(R.style.popwin_anim_style);
		
		ColorDrawable dw = new ColorDrawable(-00000);
		popupWindow.setBackgroundDrawable(dw);
		popupWindow.update();
		popupWindow.showAsDropDown(v);
		setPopwindowListener(popView, tag);

	}

	public void openUI(Class clazz) {
		startActivity(new Intent(BaseActivity.this, clazz));
//		overridePendingTransition(R.animator.act_left_out, R.animator.act_right_in);
		finish();
	}

	private void disMiss(PopupWindow pop) {
		if (pop != null && pop.isShowing()) {
			pop.dismiss();
			pop = null;
			System.out.println("pop cancel");
		}else {
			System.out.println("pop  not cancel");
		}
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		// TODO Auto-generated method stub
		if (popupWindow != null && popupWindow.isShowing()) {

			popupWindow.dismiss();

//			popupWindow = null;

		}

		return super.onTouchEvent(event);
	}

	private void setPopwindowListener(View v, int tag) {
		TextView gdatv = (TextView) v.findViewById(R.id.popmenu_tv1);

		TextView tcogtv = (TextView) v.findViewById(R.id.popmenu_tv2);
		
		TextView mltv = (TextView) v.findViewById(R.id.popmenu_tv3);
		
		TextView settv = (TextView) v.findViewById(R.id.popmenu_tv4);
		
		TextView abouttv = (TextView) v.findViewById(R.id.popmenu_tv5);
		

		ImageView gdaIV = (ImageView) v.findViewById(R.id.popmenu_image1);
		ImageView tcogIV = (ImageView) v.findViewById(R.id.popmenu_image2);
		ImageView mlIV = (ImageView) v.findViewById(R.id.popmenu_image3);
		ImageView setIV = (ImageView) v.findViewById(R.id.popmenu_image4);
		ImageView aboutIV = (ImageView) v.findViewById(R.id.popmenu_image5);

		switch (tag) {
		case 1:
			gdaIV.setVisibility(View.VISIBLE);
			break;
		case 2:
			tcogIV.setVisibility(View.VISIBLE);
			break;
		case 3:
			mlIV.setVisibility(View.VISIBLE);
			break;
		case 4:
			setIV.setVisibility(View.VISIBLE);
			break;
		case 5:
			aboutIV.setVisibility(View.VISIBLE);
			break;
		default:
			break;
		}
		if (gdaIV.getVisibility() != View.VISIBLE) {
			gdatv.setOnClickListener(this);
		}
		if (tcogIV.getVisibility() != View.VISIBLE) {
			tcogtv.setOnClickListener(this);
		}
		if (mlIV.getVisibility() != View.VISIBLE) {
			mltv.setOnClickListener(this);
		}
		if (setIV.getVisibility() != View.VISIBLE) {
			settv.setOnClickListener(this);
		}
		if (aboutIV.getVisibility() != View.VISIBLE) {
			abouttv.setOnClickListener(this);
		}
	}

	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		if (popupWindow != null ) {

			popupWindow.dismiss();

		}
		System.out.println("xxxxxrr");
		super.onPause();
		
	}
	
		
}
