package com.yuexin.cybershopper.activity;

import android.view.View;
import android.widget.ImageView;

import com.yuexin.cybershopper.R;
import com.yuexin.cybershopper.baseCase.BaseActivity;

/**
 * @author ifuumiwu
 * 商品分类
 */
public class TCOGActivity extends BaseActivity {
	private ImageView imageView;
	@Override
	protected void onClickEvent(View view) {
		// TODO Auto-generated method stub
		initPoppuWindow(findViewById(R.id.menubutton), 2);
	}

	@Override
	protected void findViewById() {
		// TODO Auto-generated method stub
		imageView = (ImageView)findViewById(R.id.menubutton);
	}

	@Override
	protected void loadViewLayout() {
		// TODO Auto-generated method stub
		setContentView(R.layout.activity_tcog);
	}

	@Override
	protected void processLogic() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void setListener() {
		// TODO Auto-generated method stub
		imageView.setOnClickListener(this);
	}

}
