package com.yuexin.cybershopper.activity;

import com.yuexin.cybershopper.baseCase.BaseActivity;
import com.yuexin.cybershopper.R;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupWindow;

public class LoginActivity extends BaseActivity {
	private ImageView menubutton;
	private ImageView searchbutton;
	private PopupWindow popupWindow;
  
	@Override
	protected void findViewById() {
		// TODO Auto-generated method stub
		menubutton = (ImageView) findViewById(R.id.menubutton);
		searchbutton = (ImageView) findViewById(R.id.searchButton);
 
	}

	@Override
	protected void loadViewLayout() {
		// TODO Auto-generated method stub
		setContentView(R.layout.activity_login);
	}

	@Override
	protected void processLogic() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void setListener() {
		// TODO Auto-generated method stub

		menubutton.setOnClickListener(this);
		searchbutton.setOnClickListener(this);

	}

	@Override
	protected void onClickEvent(View view) {
		// TODO Auto-generated method stub
		switch (view.getId()) {
		case R.id.menubutton:
			initPoppuWindow(findViewById(R.id.menubutton),0);
			break;

		default:
			break;
		}
	}

	// @Override
	// protected void onCreate(Bundle savedInstanceState) {
	// super.onCreate(savedInstanceState);
	// setContentView(R.layout.activity_main);
	// }
	//
	// @Override
	// public boolean onCreateOptionsMenu(Menu menu) {
	// // Inflate the menu; this adds items to the action bar if it is present.
	// getMenuInflater().inflate(R.menu.main, menu);
	// return true;
	// }

	/*
	 * get PopupWindow
	 */
//	private void getPopWindow(View v) {
//		if (null != popupWindow && popupWindow.isShowing()) {
//			popupWindow.dismiss();
//			return;
//		} else {
//			initPopupWindow(v);
//		}
//
//	}
//
//	private void initPopupWindow(View v) {
//		View popView = getLayoutInflater().inflate(R.layout.popw_menu, null,
//				false);
//		// WindowManager.LayoutParams lp = getWindow().getAttributes();
//		// lp.alpha = 0.5f;
//		// getWindow().setAttributes(lp);
//		popupWindow = new PopupWindow(popView, 230, 1200, true);
//		ColorDrawable dw = new ColorDrawable(-00000);
//		popupWindow.setBackgroundDrawable(dw);
//		popupWindow.update();
//		popupWindow.showAsDropDown(v);
//		TextView textView = (TextView)popView.findViewById(R.id.popmenu_tv1);
//		textView.setOnClickListener(this);
//
//	} 

 
}
